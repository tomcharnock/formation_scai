# Inferring redshifts using machine learning

In this project we will be attempting to apply several high-level machine learning techniques to astronomical data. The main objective is to build a model for the distribution of spectroscopic redshifts, *z*<sub>spec</sub>, given images of galaxies. With the limited time available we will have to use a smaller data set (and slightly less informative images) than one would normally use to attempt this problem, but great results are still possible. In total we will have 5000 examples of the data for training and validation and a small (mostly representative) subsample of 64 blind test results.

<center>
    <img width=100% src="figures/galaxies.png"/>
</center>

The galaxies chosen correspond to DR16 SDSS (and BOSS) spectroscopic measurements of galaxies, which have then been assigned spectroscopic redshift values by template fitting of the spectra. This gives us both a redshift and an error on that redshift. Since we are all (somewhat) experts in this field I have made available the SDSS object IDs, redshifts, the error on the redshift, RA and Dec as well as some information about the class of galaxy. Note that the actual data is not pulled automatically using `git clone` because of its relatively large size. First install `git-lfs` via `apt`, `yum`, `brew` or whatever and then in the project directory run
```
git lfs pull
```
This will fetch the data. Once fetched, the redshifts, errors on the redshift, RA, Dec and the rest of the properties will be available in `data/labels.npz` which contains a python dictionary with the data available via the keys `objid`, `z`, `z_err`, `ra`, `dec`, `classes`. This can be loaded using
```python
labels = np.load("data/labels.npz")
```
Note that the distribution of redshifts is not uniform (it's close to being representative of the SDSS photometric catalogue, but has been sparsely sampled).

The images are taken from the SDSS DR16 photometric catalogue and the fluxes of each of the filters have been compressed down (in both dynamic range and in filter space) to 64 x 64 pixel rgb jpgs. We can expect the pixel size to be a regular angular distance, so further away objects will appear smaller. Because these images come from the full SDSS photometry there will be multiple objects in each image - which could either make redshift inference easier *or* more difficult (images with more objects in them might come from a cluster and so every object in the image might have a similar redshift *or* the object might be far away and have many objects in front of it). This is one of the reasons why we might want to model the distribution of possible redshifts rather than make point estimates. The images themselves have been collected from SkyServer using the RA and Dec corresponding to the spectroscopic catalogue and can be loaded using
```python
images = np.load("data/images.npy")
```
The images and labels are aligned in their arrays, i.e. `image[0]` has an associated spectroscopic redshift at  `labels["z"][0]`.

### Task 1

The first task is quite open ended, but is a necessary step in any machine learning project (or science in general). We first want to look at the properties of our data and understand what biases could arise from our data set: How do the images look? What is the distribution of redshifts? What common features relate the distribution of information in the images to the distribution of redshifts?

Only by asking these questions can we start to get a handle on how we should build a model to infer spectroscopic redshifts from images of galaxies. 

Some ways of considering the correlation of features is by looking at distribution of images using unsupervised learning techniques on the data, i.e. T-SNe, density clustering (KNN, HDBScan), self-organsing maps.

For example, using T-SNe with a Euclidean metric on the flattened image vectors we can see that there are quite a few clusters with particular properties and a lot of outliers (left hand plot), but these are distributed rather smoothly in redshift space (as seen on the right hand plot). Of course a different metric would give a very different interpretation, and a different method could say something quite different again. Expect to take at least an hour getting to grips with the data. I personally spent about 2 hours on this task and used about 6 different unsupervised learning methods each with several different metrics - this made the main objective much easier.

![](figures/T-SNe_euclidean.png)

Analysising the distribution of data will give some indications about the number of different types of objects in the data (spirals, ellipticals, mergers, etc.) and also show how well these different objects can be distinguished using different types of metrics, i.e. does translational or rotational symmetry need to be considered when processing the data, etc. Using our gained knowledge is it then possible to find how different features of the images relate to their corresponding different redshifts?

Of course your eyes can help you somewhat, but 5000 images is still a lot to deal with, so using histograms or other ways to visualise distributions will also be very useful here.

Imaginary points will be given for the prettiest visualisation which is also informative about the distribution of features in the data and how it relates to the distribution of redshifts for each image.

### Task 2

This is the main objective of the project. Can you build a model of the distribution of spectroscopic redshifts using machine learning techniques and images of galaxies. In the last few years we might have trained a supervised neural network and made predictions which look like:

![](figures/regression_model_2_prediction.png)

Spectroscopic redshifts (with errorbars - they're smaller than the point size) are plotted on the *x*-axis and the network outputs (which would be the mean prediction of the neural network if using the mean squared error as a loss function) on the *y*-axis. *Hopefully,* all the points would lie on the dotted line if the model was sufficient. However, for scientific purposes, a plot like this doesn't indicate how good the fit is and so extrapolating the results to events without a spectroscopic counterpart could lead to severe biasing. So what we want is a way to evaluate how likely any result is - as well as making a point estimate. 

Using what you have learned from the first part of the task, you can start to think about which methods might work best for building your model. Remember the task is to infer the spectroscopic redshift from galaxy images and as part of this model we need the likelihood of the spectroscopic redshift. So to build this model we need some probabilistic measure. Note that due to the limited time limits we don't necessarily expect that probabilistic model to show epistemic uncertainties, i.e. the error in the model, although extra (imaginary) points will be given to anyone who manages this. There are many possible machine learning methods to use, for example:
- Random forests
- Binned redshifts and logistic regression
- Regressive neural network (and evaluating the loss function as the probabilistic model)
- Gaussian processes
- Variational inference
- Image feature, redshift correlation
- Any other methods

Here we show the likelhood of spectroscopic redshifts given our input images in blue and the black dashed line (and grey bands) show the measured spectroscopic redshift and 1σ uncertainties (although uncertainties  are so small you probably can't see them).

![](figures/error_model_prediction.png)

We can see that in general the model is able to predict the spectroscopic redshift pretty well. The error is much greater than the spectroscopic error - but this is expected because there is a lot less information in the photometric images than there was in the spectroscopic observations used to make the initial mesurement. Note in this model the epistemic uncertainty of the model isn't included, but you could expect the large redshift measurements to have a much greater variance than the low redshifts.

Remember to think about the distribution of features in the data and the ways to learn and extract information from the images. What sorts of machine learning could be used? Convolutional kernels for translationally invariant features, maybe data augmentation to learn about rotationally invariant features, clever choices of metrics, etc.

Once you think you have a great model, ask an advisor to test it on the blind test data and see how it does!

### Task 3

If any one manages to finish task 2 then there is an extra bonus task. Can you use machine learning to generate new images of galaxies? This is a tricky task with the amount of data because there are only a limited distribution of features to learn how to extrapolate. However, using the knowledge that you've gained about the distribution of features in the images, it is possible to make a network (using methods such as VAE, Wasserstein GANs, or variational inference) that can generate completely new galaxy images. Note that these might be a bit crazy without any constraint:

![](figures/generated_images.png)

However, it is easiest to try and learn to generate images which recreate unseen test images and that can be sampled to get slight differences.

![](figures/VAE.png)

If you are able to generate these new images let's then modify the existing test images with new features. For example, by being able to control the possible distribution of outputs via, perhaps, a latent space - we can make images that go from their original form to something with much more complex features and different properties... (including redshift!)

![](figures/VAE_modify.png)

## Good luck and have fun!
